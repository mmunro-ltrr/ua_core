# UA Core *(now deprecated: use [UAQS Core](https://bitbucket.org/ua_drupal/uaqs_core) instead)*

Provides core dependencies and features for the [UA Quickstart](https://bitbucket.org/ua_drupal/ua_quickstart) Drupal Distribution (or any other distributions wishing to make use of it).

## Packaged Dependencies

When this module is used as part of a Drupal distribution (such as [UA Quickstart](https://bitbucket.org/ua_drupal/ua_quickstart)), the following dependencies will be automatically packaged with the distribution.

### Drupal Contrib Modules

#### Common Contrib Module Dependencies
- [Chaos tool suite (ctools)](https://www.drupal.org/project/ctools)
- [Elements](https://www.drupal.org/project/elements)
- [Entity API](https://www.drupal.org/project/entity)
- [HTML5 Tools](https://www.drupal.org/project/html5_tools)
- [jQuery Update](https://www.drupal.org/project/jquery_update)
- [Libraries API](https://www.drupal.org/project/libraries)
- [Pathauto](https://www.drupal.org/project/pathauto)
- [Token](https://www.drupal.org/project/token)
- [Views](https://www.drupal.org/project/views)

#### Common Contrib Field Modules
- [Automatic Nodetitles](https://www.drupal.org/project/auto_nodetitle)
- [Date](https://www.drupal.org/project/date)
- [Email Field](https://www.drupal.org/project/email)
- [Entity reference](https://www.drupal.org/project/entityreference)
- [Field collection](https://www.drupal.org/project/field_collection)
- [Field Group](https://www.drupal.org/project/field_group)
- [Link](https://www.drupal.org/project/link)

#### Features-related Contrib Modules
- [Default config](https://www.drupal.org/project/defaultconfig)
- [Features](https://www.drupal.org/project/features)
- [Migrate](https://www.drupal.org/project/migrate)
- [Strongarm](https://www.drupal.org/project/strongarm)

### Drupal Contrib Themes

- [Zen](https://www.drupal.org/project/zen)
